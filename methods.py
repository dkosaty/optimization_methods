from numpy.linalg import norm

from functions import df


def gradient_descent(x0, alpha, eps):
    curr_x = x0.copy()

    while True:
        prev_x = curr_x.copy()

        curr_x -= alpha * df(curr_x)

        if norm(curr_x - prev_x) < eps:
            break

    return curr_x


def heavy_ball(x0, alpha, beta, eps):
    prev_x, curr_x = x0.copy(), x0.copy()

    while True:
        new_x = curr_x - alpha * df(curr_x) + beta * (curr_x - prev_x)

        prev_x, curr_x = curr_x.copy(), new_x.copy()

        if norm(curr_x - prev_x) < eps:
            break

    return curr_x


def fast_gradient(x0, alpha, beta, eps):
    prev_x, curr_x = x0.copy(), x0.copy()

    while True:
        new_x = curr_x - alpha * df(curr_x + beta * (curr_x - prev_x)) + beta * (curr_x - prev_x)

        prev_x, curr_x = curr_x.copy(), new_x.copy()

        if norm(curr_x - prev_x) < eps:
            break

    return curr_x
