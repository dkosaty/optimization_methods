import numpy as np


# def f(x):
#     """
#     Sphere function
#
#     f(x*) = 0 at x* = (0, ..., 0)
#     """
#
#     return np.sum(x * x)


# def f(x):
#     """
#     Styblinski–Tang function
#
#     f(x*) = -39.16599n at x* = (-2.903534, ..., -2.903534), where n - space dimension
#     """
#
#     if any(abs(x) > 5.0):
#         raise ValueError
#
#     return np.sum(0.5 * x**4 - 8.0 * x**2 + 2.5 * x)


def f(x):
    """
    Rosenbrock function

    f(x*) = 0 at x* = (1, ..., 1)
    """

    return np.sum([pow(1 - x[i], 2) + 100 * pow(x[i + 1] - x[i] * x[i], 2) for i in range(x.shape[0] - 1)])


def df(x):
    gradient = np.ndarray(x.shape, dtype=float)

    for i in range(x.shape[0]):
        delta = np.zeros(x.shape[0])

        delta[i] = 1E-6

        gradient[i] = 0.5 * (f(x + delta) - f(x - delta)) / delta[i]

    return gradient
