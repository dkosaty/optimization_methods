from time import time

import numpy as np

from methods import *


def main():
    x0 = np.array([0.0, 0.0, 0.0])

    alpha, beta, eps = 1E-3, 1E-3, 1E-6

    try:
        t = time()
        x = gradient_descent(x0, alpha, eps)
        # x = heavy_ball(x0, alpha, beta, eps)
        # x = fast_gradient(x0, alpha, beta, eps)
        t = time() - t
    except ValueError:
        return

    print(t, x)


if __name__ == '__main__':
    main()
